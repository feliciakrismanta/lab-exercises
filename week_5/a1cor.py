PRINT_TOPIC = 1
APPLICATION_TOPIC = 3
NO_HELP_TOPIC = -1
BUTTON_PRINT = 4


class HelpHandler(object):

    def __init__(self, successor=0, topic=NO_HELP_TOPIC):
        self._successor = successor
        self._topic = topic

    def has_help(self):
        return self._topic != NO_HELP_TOPIC

    def handle_help(self):
        if self._successor != 0:
            self._successor.handle_help()

    def set_handler(self, h, t):
        self._successor = h
        self._h = t


class Widget(HelpHandler):
    def __init__(self, w, t):
        super(Widget,self).__init__(w,t)

class Application(HelpHandler):
    def __init__(self, t):
        super(Application,self).__init__(0,t)
    def handle_help(self):
        print ("Application Help")

class Dialog(Widget):
    def __init__(self, h, t):
        super(Dialog,self).__init__(h,t)
        self.set_handler(h,t)
    def handle_help(self):
        if self.has_help():
            print ("Dialog Help")
        else:
            return super(Dialog, self).handle_help()

class Button(Widget):
    def __init__(self, d, t):
        super(Button,self).__init__(d,t)
    def handle_help(self):
        if self.has_help():
            print ("Button Help")
        else:
            return super(Button, self).handle_help()

def main():
    application = Application(APPLICATION_TOPIC)
    dialog = Dialog(application, PRINT_TOPIC)
    button = Button(dialog, NO_HELP_TOPIC)

    # Invoking help in the chain
    button.handle_help()


if __name__ == "__main__":
    main()
