# public class BridgePatternDemo {

#     public static void main(String[] args) {
#         Shape redCircle = new Circle(100,100, 10, new RedCircle());
#         Shape greenCircle = new Circle(100,100, 10, new GreenCircle());

#         redCircle.draw();
#         greenCircle.draw();
#     }
# }
def main():
    redCircle = Circle(100,100,10, RedCircle())
    greenCircle = Circle(100,100,10, GreenCircle())
    redCircle.draw()
    greenCircle.draw()  

# abstract class Shape {

#     protected DrawAPI drawAPI;

#     protected Shape(DrawAPI drawAPI) {
#         this.drawAPI = drawAPI;
#     }

#     public abstract void draw();	
# }

class Shape:
    def __init__(self, drawAPI):
        self.drawAPI = drawAPI

    def draw(self):
        pass

# class Circle extends Shape {

#     private int x; 
#     private int y;
#     private int radius;

#     public Circle(int x, int y, int radius, DrawAPI drawAPI) {
#         super(drawAPI);
#         this.x = x;  
#         this.y = y;  
#         this.radius = radius;
#     }

#     public void draw() {
#         drawAPI.drawCircle(radius, x, y);
#     }
# }

class Circle(Shape):
    def __init__(self, x,  y,  radius ,drawAPI):
        super().__init__(drawAPI)
        self._x = x  
        self._y = y
        self._radius = radius
        
    def draw(self):
        self.drawAPI.drawCircle(self._radius, self._x, self._y);

# interface DrawAPI {

#     public void drawCircle(int radius, int x, int y);
# }
class DrawAPI:
    def drawCircle(self, radius, x, y):
        pass


# class RedCircle implements DrawAPI {
#     public void drawCircle(int radius, int x, int y) {
#         System.out.println("Drawing Circle[ color: red, radius: " +
#                             radius + ", x: " + x + ", " + y + "]");
#     }
# }
class RedCircle(DrawAPI):
    def drawCircle(self, radius, x, y):
        print("Drawing Circle[ color: red, radius: " , radius , ", x: " , x , ", " , y , "]")

# class GreenCircle implements DrawAPI {
#     public void drawCircle(int radius, int x, int y) {
#         System.out.println("Drawing Circle[ color: green, radius: " +
#                             radius + ", x: " + x + ", " + y + "]");
#     }
# }

class GreenCircle(DrawAPI):
    def drawCircle(self, radius, x, y):
        print("Drawing Circle[ color: green, radius: " , radius , ", x: " , x , ", " , y , "]")

main();