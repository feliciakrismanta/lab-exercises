from week4 import SimpleItem, CompositeItem, make_item, make_composite

def main():
    pencil = make_item("Pencil", 0.40)
    ruler = make_item("Ruler", 1.60)
    eraser = make_item("Eraser", 0.20)
    pencilSet = make_composite("Pencil Set", pencil, ruler, eraser)
    box = make_item("Box", 1.00)
    boxedPencilSet = make_composite("Boxed Pencil Set", box,pencilSet)
    boxedPencilSet.add(pencil)
    for item in (pencil, ruler, eraser, pencilSet, boxedPencilSet):
        item.print()
        
if __name__ == "__main__":
    main()