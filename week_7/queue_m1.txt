

Program memilikki 4 fixed coin values untuk nantinya dirandom. coin merupakan namedtuple. Di module main di instantiate timer. Program ini menunjukkan concurrency menggunakan library built in python yaitu queue. 2 proses yang berjalan adalah reader dan writer. Di program sudah di set agar xoin insertion dilakukan 100kali dan reader menunggu hingga 25 loop baru menterminate process lalu program akan kembali menjadi single process.


[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (100)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (100)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (100)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (500)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (100)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (200)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (500)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (1000)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (100)
[Process Reader 1] Read coin (200)
[Process Reader 1] Read coin (100)
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Waiting for new items...
[Process Reader 1] Total value read: 49300
[Process Reader 1] Exiting...
Put coin (100) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (100) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (500) into queue
Put coin (100) into queue
Put coin (100) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (100) into queue
Put coin (200) into queue
Put coin (500) into queue
Put coin (500) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (100) into queue
Put coin (500) into queue
Put coin (200) into queue
Put coin (1000) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (500) into queue
Put coin (100) into queue
Put coin (500) into queue
Put coin (200) into queue
Put coin (200) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (500) into queue
Put coin (100) into queue
Put coin (500) into queue
Put coin (200) into queue
Put coin (500) into queue
Put coin (200) into queue
Put coin (200) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (100) into queue
Put coin (500) into queue
Put coin (500) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (100) into queue
Put coin (200) into queue
Put coin (200) into queue
Put coin (500) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (1000) into queue
Put coin (500) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (500) into queue
Put coin (200) into queue
Put coin (100) into queue
Put coin (200) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (500) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (100) into queue
Put coin (100) into queue
Put coin (500) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (200) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (100) into queue
Put coin (200) into queue
Put coin (100) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (1000) into queue
Put coin (200) into queue
Put coin (100) into queue
Put coin (200) into queue
Put coin (100) into queue
Total value written: 49300
Total running time: 32.703354835510254