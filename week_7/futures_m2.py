from collections import namedtuple
from concurrent.futures import ProcessPoolExecutor, as_completed
import multiprocessing as mp
import random
import time

VALUES = (100, 200, 500, 1000)
Coin = namedtuple('Coin', ['value'])


def reader(coin):
    print("[Process {}] Read coin ({})".format(
                  mp.current_process().name, str(coin.value)))
    return coin.value


def writer(count, nprocs):
    writer_sum = 0
    futures = []
    executor=ProcessPoolExecutor(nprocs)
    for ii in range(count):
        coin = Coin(random.choice(VALUES))
        writer_sum += coin.value

        print("Put coin ({}) into queue".format(coin.value))
        time.sleep(random.random() * 0.50)
        futures.append(executor.submit(reader, coin))

    sumval = 0
    for f in as_completed(futures):
        sumval += f.result()
    print("[Process {}] Total value read: {}".format(mp.current_process().name, sumval))
    print('Total value written: ' + str(writer_sum))
    print("[Process {}] Exiting...".format(mp.current_process().name))
    print('Total value written: ' + str(writer_sum))


if __name__ == '__main__':
    start_time = time.time()
    count = 100
    writer(count, 2)
    end_time = time.time()

    print('Total running time: ' + str(end_time - start_time))
