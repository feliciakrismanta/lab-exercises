import multiprocessing as mp

def factorize_naive(n):
    """ A naive factorization method. Take integer 'n', return list of
        factors.
    """
    if n < 2:
        return []
    factors = []
    p = 2

    while True:
        if n == 1:
            return factors

        r = n % p
        if r == 0:
            factors.append(p)
            n = n // p
        elif p * p >= n:
            factors.append(n)
            return factors
        elif p > 2:
            # Advance in steps of 2 over odd numbers
            p += 2
        else:
            # If p == 2, get to 3
            p += 1
    assert False, "unreachable"

def reader(queue):
    termination_threshold = 25
    termination_count = 0

    while termination_count < termination_threshold:
        if queue.empty():
            termination_count += 1
        else:
            termination_count = 0
            coin = queue.get()
            print (coin, factorize_naive(coin))


def writer(values, queue):
    
    for ii in values:
        queue.put(ii)


if __name__ == '__main__':
    nums = [25, 36, 42, 88, 99]
    queue = mp.Queue()  # Queue class from multiprocessing module

    reader_p1 = mp.Process(target=reader, name='Reader 1', args=(queue,))
    reader_p1.daemon = True
    reader_p1.start()

    writer(nums, queue)
    reader_p1.join()
