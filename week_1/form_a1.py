class Form:
    _type = ""
    _input = ""
    _button = ""

    def __init__(Class, type, input, button):
        Class._type = type
        Class._input = input
        Class._button = button

    def getType(Class):
        return Class._type.getType()

    def getInput(Class):
        return Class._input.getInput()

    def getButton(Class):
        return Class._button.getButton()
        
    set = __init__

class EntryType:
    _type = ""

    def __init__(Class):
        Class._type = "entry"

    def getType(Class):
        return Class._type

class EntryInput:
    _input = ""

    def __init__(Class):
        Class._input = "data"

    def getInput(Class):
        return Class._input

class EntryButton:
    _button = ""

    def __init__(Class):
        Class._button = "save"
    
    def getButton(Class):
        return Class._button

## Mandatory checklist no 2 ###
def create_form(factory):
   form = factory.create_form()
   fType = factory.create_type()
   fInput = factory.create_input()
   fButton = factory.create_button()
   form.set(fType, fInput, fButton)
   return form

class InquiryFormFactory:

    class InquiryType:
        _type = ""

        def __init__(Class):
            Class._type = "inquiry"

        def getType(Class):
            return Class._type

    class InquiryInput:
        _input = ""

        def __init__(Class):
            Class._input = "query"

        def getInput(Class):
            return Class._input

    class InquiryButton:
        _button = ""

        def __init__(Class):
            Class._button = "search"
        
        def getButton(Class):
            return Class._button

    def create_form():
        return Form(None,None,None)

    @classmethod
    def create_type(Class):
        return Class.InquiryType()

    @classmethod
    def create_input(Class):
        return Class.InquiryInput()

    @classmethod
    def create_button(Class):
        return Class.InquiryButton()


### Mandatory checklist no 3 ###
#def create_form(builder):
#    builder.create_type()
#    builder.create_input()
#    builder.create_button()
#    return builder.form()

def main():
    # inquiryType = InquiryType()
    # inquiryInput = InquiryInput()
    # inquiryButton = InquiryButton()

    # inquiryForm = Form(inquiryType, inquiryInput, inquiryButton)
    # print("Form type ", inquiryForm.getType())
    # print("Form input ", inquiryForm.getInput())
    # print("Form button ", inquiryForm.getButton())

    ## Mandatory checklist no 1 ###
    entryType = EntryType()
    entryInput = EntryInput()
    entryButton = EntryButton()

    entryForm = Form(entryType, entryInput, entryButton)
    print("Form type ", entryForm.getType()) #will print "entry"
    print("Form input ", entryForm.getInput()) #will print "data"
    print("Form button ", entryForm.getButton()) #will print "save"

    ## Mandatory checklist no 2 ###
    inquiryForm = create_form(InquiryFormFactory)
    print("Form type ", inquiryForm.getType())
    print("Form input ",inquiryForm.getInput())
    print("Form button ",inquiryForm.getButton())

    ### Mandatory checklist no 3 ###
    #inquiryForm = create_form(InquiryFormBuilder())
    #print("Form type ", inquiryForm.getType())
    #print("Form input ", inquiryForm.getInput())
    #print("Form button ", inquiryForm.getButton())

main()
