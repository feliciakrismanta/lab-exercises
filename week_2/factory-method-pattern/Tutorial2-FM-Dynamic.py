#	Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

def main():
	print ("Masukkan n besar untuk board ")
	n = input();
	while ( (not n.isdigit()) or 1>int(n)):
		n = input();
	board = Boardnxn(int(n))
	print (board)

class AbstractBoard:
	
	def __init__(self, rows, columns):
		self.board = [[None for _ in range(columns)] for _ in range(rows)]
		self.populate_board()
		
	def populate_board(self):
		raise NotImplementedError()

	def __str__(self):
		squares = []
		for x, row in enumerate(self.board):
			for y, column in enumerate(self.board):
				squares.append(self.board[x][y])
			squares.append("\n")
		return "".join(squares)

class Boardnxn(AbstractBoard):

	def __init__(self,n):
		self.n = n
		super().__init__(n, n)

	def populate_board(self):
		# Asumsi base baris 1
		for row in range(self.n):
			if (row % 2):
				for column in range(self.n):
					if (column % 2): self.board[row][column] = Vuvu()
					else: self.board[row][column] = Uvuv()
			else:
				for column in range(self.n):
					if (column % 2): self.board[row][column] = Circle()
					else: self.board[row][column] = Cross()
					

class Piece(str):
	__slots__ = ()
	
class Circle(Piece):
	__slots__ = ()
	def __new__(Class):
		return super().__new__(Class, "o")

class Cross(Piece):
	__slots__ = ()
	def __new__(Class):
		return super().__new__(Class, "x")

class Vuvu(Piece):
	__slots__ = ()
	def __new__(Class):
		return super().__new__(Class, "v")

class Uvuv(Piece):
	__slots__ = ()
	def __new__(Class):
		return super().__new__(Class, "u")


if __name__ == "__main__":
    main()
				
