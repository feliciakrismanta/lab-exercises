class SubscriberOne:

    def __init__(self, name):
        self.name = name

    def update(self, message):
        print('{} got message "{}"'.format(self.name, message))


class SubscriberTwo:

    def __init__(self, name):
        self.name = name

    def receive(self, message):
        print('{} got message "{}"'.format(self.name, message))


class Publisher:

    def __init__(self):
        self.subscribers = dict()

    def register(self, who, callback=None):
        self.subscribers[who] = callback

    def unregister(self, who):
        self.subscribers.pop(who)

    def notify(self, message):
         for subs in self.subscribers:
            if self.subscribers[subs]:
                self.subscribers[subs](message)
            else:
                subs.update(message)
